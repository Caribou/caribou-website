## Summary

Caribou-website is the deployment tool for the Caribou project’s website. It collects documentation from the Peta-Caribou, Boreal, and Peary projects, then builds and deploys the website. You can access the website at this [link](https://caribou-project.docs.cern.ch).


