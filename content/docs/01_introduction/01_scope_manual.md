---
# SPDX-FileCopyrightText: 2022 CERN and the Caribou authors
# SPDX-License-Identifier: CC-BY-4.0
title: "Scope of this Manual"
weight: 1
---

This manual provides comprehensive documentation for the Caribou DAQ system, aimed at enabling users to fully understand, utilize, and integrate their detector prototypes with the system. It covers the Caribou architecture and each of its core components in detail, ensuring users have a clear understanding of the system’s capabilities and configuration options.

The manual includes:

1. **System Architecture Overview**

An introduction to the Caribou DAQ system’s modular design, including the hardware, firmware, and software stack, and the interconnections between these components. This section gives users a foundational understanding of how Caribou operates and supports flexible detector prototyping.

2. **Detailed Component Descriptions**

- Hardware: An in-depth look at the Control and Readout (CaR) board, its power supplies, data interfaces, signal processing units, and how it connects to the Zynq System-on-Chip (SoC).

- Firmware: Documentation of the modular firmware architecture, with details on the various available re-usable block, their functions and configuration options. Users will gain insight into reusable components and how these can be tailored for specific applications.

- Software: A guide to the Peary DAQ software, including its Hardware Abstraction Layer (HAL), configuration options, and control interfaces. This section covers the structure and use of Peary’s CLI and API, enabling easy interaction with the hardware.

3. **User Instructions**
Step-by-step instructions to help users set up the system, connect and configure their device, and begin collecting data. This includes build of the Petalinux image, generation of Boreal bitstreams, configuration of Peary software for specific detector needs, and guidelines for testing and troubleshooting.

4. **Integration and Testing**
Guidance on seamlessly integrating custom detector prototypes with Caribou, including adapting firmware and software settings to meet detector-specific requirements. Users will find procedures for validating hardware connections, verifying firmware modules, and ensuring data readout compatibility.

This manual is designed as a complete resource to make the Caribou system accessible to users with diverse needs, from initial setup through advanced customization and testing of pixel detector prototypes. Each section provides the essential information and practical steps necessary for leveraging Caribou’s flexibility and modularity, making it easy to adapt the system for different research and development applications.