---
# SPDX-FileCopyrightText: 2022 CERN and the Caribou authors
# SPDX-License-Identifier: CC-BY-4.0
title: "Support and Issues"
weight: 2
---

For support with the Caribou system, users are encouraged to join the [Mattermost group](https://mattermost.web.cern.ch/caribou) dedicated to Caribou, where developers and fellow users can assist with questions and provide guidance. Additionally, direct support is available by contacting the development team via *caribou-developers@cern.ch*.

If you encounter any issues or bugs, please report them through the Mattermost channel of the component concerned or by opening an issue on its corresponding Caribou GitLab repository, where developers actively track and address reported problems. Engaging through these channels ensures that the development team and user community can provide timely support and collaboratively resolve issues.