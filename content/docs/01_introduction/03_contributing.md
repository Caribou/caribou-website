---
# SPDX-FileCopyrightText: 2022 CERN and the Caribou authors
# SPDX-License-Identifier: CC-BY-4.0
title: "Contributing"
weight: 3
---

If you are interested in contributing to the Caribou project or joining the development effort, we encourage you to reach out to the core development team. Prospective contributors are welcome to join as part of the core development team or to propose new features and enhancements for potential integration. Developers interested in presenting their work or discussing ideas for improvements can be invited to one of the regular developers' meetings to showcase their contributions.

To get started, please contact the development team at *caribou-developers@cern.ch*. This will connect you with the team to discuss how your contributions can align with ongoing efforts and the future direction of Caribou. We value community involvement and are excited to support and incorporate innovative ideas from new contributors.
