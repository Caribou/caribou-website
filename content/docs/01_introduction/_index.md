---
# SPDX-FileCopyrightText: 2022-2023 CERN and the Caribou authors
# SPDX-License-Identifier: CC-BY-4.0
title: "Introduction"
description: "A brief introduction to the Caribou project"
weight: 1
---

Caribou is a versatile and modular data acquisition (DAQ) system designed for prototyping silicon pixel detectors. Built around the Control and Readout (CaR) board, Caribou provides an adaptable hardware platform that includes all essential interfaces and power supplies needed for a range of detector ASICs. 
At its core, Caribou leverages a Zynq System-on-Chip (SoC) board running a Yocto-based Linux OS with the Peary DAQ software and a user-specific Boreal FPGA firmware. This setup offers a powerful combination of programmable logic and high-level software control, making Caribou suitable for a variety of detector prototypes.

The Caribou system streamlines the process of testing and debugging new detectors by providing configurable firmware blocks, comprehensive software interfaces, and reusable hardware components. This flexibility makes it easy to tailor Caribou to different detector needs, reducing development time and enhancing productivity. Originally developed for the ATLAS Inner Tracker (ITk) and CLIC vertex detector projects, Caribou’s open-source architecture now supports a broad range of applications, providing a ready-to-use, adjustable, and efficient DAQ framework for particle physics research.

<div align="left">
    <img src="./images/caribou.pdf" alt="Caribou System" width="60%">
</div>

As most of the pixel detectors share similar power, control and readout concepts, the system is designed with the objective of minimizing the work on the base test-system and maximizing the focus on device integration and testing. It is primarily used within [AIDAinnova](https://aidainnova.web.cern.ch) and other collaborative frameworks ([CERN EP R&D](https://ep-rnd.web.cern.ch), [DRD3](https://drd3.web.cern.ch), [Tangerine](https://fe.desy.de/fec/projects/high_energy_physics/tangerine/)) for laboratory and high-rate beam tests and easy integration of new silicon-pixel-detector prototypes. I is developped by a collaborative effort valuing user contributions but lead by five main institutes: [CERN](https://home.cern), [DESY](https://www.desy.de), [BNL](https://www.bnl.gov/world/), [ORNL](https://www.ornl.gov) and [Carleton Univesity](https://carleton.ca).





