---
# SPDX-FileCopyrightText: 2022 CERN and the Caribou authors
# SPDX-License-Identifier: CC-BY-4.0
title: "Publications"
menu:
  main:
    weight: 20
    pre: <i class='fa-solid fa-clipboard'></i>
---

{{< blocks/lead height="auto" color="secondary">}}
<h2 style="padding-bottom: 1em;">
    Publications
</h2>
<span class="caribou-lead-text">
    Reference publications, presentation slide decks and tutorial resources on Caribou.
</span>
{{< /blocks/lead >}}

<div class="container l-container--padded">

# Reference Publications

{{< citation title="**Caribou - A versatile data acquisition system for silicon pixel detector prototyping**" authors="Y. Otarid et al." reference="[arXiv:2502.03903v1](https://arxiv.org/pdf/2502.03903)" >}}

{{< citation title="**Status and recent extensions of the Caribou DAQ System for picosecond timing**" authors="E. Buschmann et al." journal="JINST 18 C02005, Vol 18, Feb 2023" reference="[doi:10.1088/1748-0221/18/02/C02005](https://iopscience.iop.org/article/10.1088/1748-0221/18/02/C02005/pdf), [arXiv:2210.13139](https://arxiv.org/pdf/2210.13139)" >}}

{{< citation title="**Caribou - A versatile data acquisition system**" authors="T. Vanat et al." journal="Topical Workshop on Electronics for Particle Physics TWEPP2019" reference="[PoS:TWEPP2019](https://pos.sissa.it/370/100/pdf)" >}}

{{< citation title="**Development of a modular test system for the silicon sensor R&D of the ATLAS Upgrade**" authors="H. Liu et al." journal="JINST 12 P01008" reference="[10.1088/1748-0221/12/01/P01008](https://iopscience.iop.org/article/10.1088/1748-0221/12/01/P01008/pdf)" >}}

{{< citation title="**A multi-chip data acquisition system based on a heterogeneous system-on-chip platform**" authors="A. Fiergolski" journal="Springer Proc. Phys. 212 (2018) 303-308" reference="[10.1007/978-981-13-1313-4_57](https://cds.cern.ch/record/2272077/files/CLICdp-Conf-2017-012.pdf)" >}}

# Reference Presentations

{{< citation title="**Caribou: Overview of the system, recent developments and future plans**" authors="Y. Otarid" journal="CERN DRD3 Workshop Nov 2024" reference="[Presentation](./files/drd3_project_meeting_291124.pdf)" >}}

{{< citation title="**Caribou: A Versatile Data Acquisition System for Silicon Pixel Detector Characterization**" authors="Y. Otarid" journal="CERN EP-R&D Seminar Oct 2024" reference="[Presentation](./files/younes_otarid_ep_rd_seminar_141024.pdf)" >}}

</div>