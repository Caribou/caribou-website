---
# SPDX-FileCopyrightText: 2022 CERN and the Caribou authors
# SPDX-License-Identifier: CC-BY-4.0

title: "Community"
menu:
  main:
    weight: 70
    pre: <i class='fa-solid fa-users'></i>
---

{{< blocks/lead height="auto" color="secondary" >}}
<h2 style="padding-bottom: 1em;">
    Community
</h2>
<span class="caribou-lead-text">
    Caribou is a collaborative, open source test-system. This page contains directions where to find help and support, and how to get actively involved in the development.
</span>
{{< /blocks/lead >}}

<div class="container l-container--padded">
<div class="row">
{{% tool title="User Manual" icon="fas fa-book" url="/docs/" desc="Comprehensive user manual for Caribou" %}}
{{% tool title="Mattermost" icon="fas fa-comments" url="https://mattermost.web.cern.ch/caribou" desc="Discussion channel for developers and users" %}}
{{% tool title="User Forum" icon="fa-solid fa-network-wired" url="https://caribou-forum.web.cern.ch/" desc="Forum for questions and discussions" %}}
</div>
<div class="row">
{{% tool title="Mailing lists" icon="fas fa-envelope" url="https://e-groups.cern.ch/e-groups/EgroupsSearch.do?searchValue=caribou" desc="Mailing list for Caribou (via CERN e-groups, very low traffic)" %}}
{{% tool title="GitLab" icon="fab fa-gitlab" url="https://gitlab.cern.ch/Caribou" desc="Main development repository (CERN GitLab, requires CERN account for write access)" %}}
{{% tool title="Issue Tracker" icon="fas fa-bug" url="https://gitlab.cern.ch/groups/Caribou/-/issues" desc="List of known issues and bugs" %}}
</div>
<div class="row">
<!-- {{% tool title="Developer manual" icon="fas fa-laptop-code" url="/docs/10_development/" desc="Sections of the user manual related to module development and CI" %}} -->
</div>
</div>
